﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MarsRoverTest.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label><br /><br />
            <h1>Rovers Current Position On Planet <%=oprPlanetMars.PlanetName %></h1><br />
            <asp:Label ID="lblRovers" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
