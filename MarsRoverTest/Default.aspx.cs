﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CLOprPlanetX;

namespace MarsRoverTest
{
    public partial class Default : System.Web.UI.Page
    {
        public OprPlanet oprPlanetMars;

        protected void Page_Load(object sender, EventArgs e)
        {
            RectanglePlateau plateauDef = new RectanglePlateau(new TwoDCoordinate(0, 0), new TwoDCoordinate(5, 5));
            Rover[] rovers = {
                new Rover(new TwoDCoordinate(1, 2), CompassDirection.N, "LMLMLMLMM"),
                new Rover(new TwoDCoordinate(3, 3), CompassDirection.E, "MMRMMRMRRM"),
                new Rover(new TwoDCoordinate(3, 3), CompassDirection.E, "MMRMMRMRRMMXY"),
                new Rover(new TwoDCoordinate(1, 2), CompassDirection.N, "LMLMLMLMMLMMMM"),
                new Rover(new TwoDCoordinate(4, 4), CompassDirection.E, ""),
                new Rover(new TwoDCoordinate(1, 1), CompassDirection.S,"MMM")
            };

            //show passed inputs
            //show original(passed) plateau coordinates
            lblRovers.Text = "Original Plateau " + plateauDef.BottomLeft.x.ToString() + " " + plateauDef.BottomLeft.y.ToString() + " " + plateauDef.TopRight.x.ToString() + " " + plateauDef.TopRight.y.ToString();
            lblRovers.Text += "<br/>Rover(s) Given Positions-";
            for (int i = 0; i < rovers.Length; i++) //get current position FOR EACH rover
            {
                //show original rover position
                lblRovers.Text += "<br/><b>Rover " + (i + 1).ToString() + " : </b>";
                lblRovers.Text += rovers[i].CurrentPosition.x.ToString() + " " + rovers[i].CurrentPosition.y.ToString() + " " + rovers[i].RoverDirection.ToString() + " " + rovers[i].Route;
            }

            oprPlanetMars = new OprPlanet("Mars", plateauDef, rovers);
            oprPlanetMars.MoveRoversAsPerDirections();

            ShowRoversCurrentLocations(plateauDef, rovers);
        }

        private void ShowRoversCurrentLocations(RectanglePlateau plateauDef, Rover[] rovers)
        {
            //show redefined plateau coordinates
            lblRovers.Text += "<br/><br/>Redefined Plateau " + oprPlanetMars.PlateauDef.BottomLeft.x.ToString() + " " + oprPlanetMars.PlateauDef.BottomLeft.y.ToString() + " " + oprPlanetMars.PlateauDef.TopRight.x.ToString() + " " + oprPlanetMars.PlateauDef.TopRight.y.ToString();
            lblRovers.Text += "<br/>Rover(s) Current Positions-";

            for (int i = 0; i < rovers.Length; i++) //get current position FOR EACH rover
            {
                //show rover position
                lblRovers.Text += "<br/><b>Rover " + (i + 1).ToString() + " : </b>";

                if (oprPlanetMars.IsRoverPositionValid(oprPlanetMars.Rovers[i],plateauDef)) { 

                    lblRovers.Text += oprPlanetMars.Rovers[i].CurrentPosition.x.ToString() + " " + oprPlanetMars.Rovers[i].CurrentPosition.y.ToString() + " " + oprPlanetMars.Rovers[i].RoverDirection.ToString();
                }
                else
                {
                    lblRovers.Text += "Invalid coordinates!"; //this means this particular rover's original coordinates do not fall inside given plateau coordinates
                }
            }
        }
    }
}