﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLOprPlanetX
{
    public class OprPlanet
    {
        public OprPlanet(string planetName, RectanglePlateau plateauDef, Rover[] rovers)
        {
            PlanetName = planetName;
            PlateauDef = plateauDef;
            Rovers = rovers;
        }

        public string PlanetName { get; }

        public RectanglePlateau PlateauDef { get; }

        public Rover[] Rovers { get; }

        /// <summary>
        /// Determines whether rover is present inside given plateau coordinates or not.
        /// </summary>
        /// <param name="rover">Rover object for which the position validity is to be checked.</param>
        /// <param name="plateauDef">Plateau object against which the rover position is being checked.</param>
        /// <returns></returns>
        public bool IsRoverPositionValid(Rover rover, RectanglePlateau plateauDef)
        {
            if ((rover.CurrentPosition.x >= plateauDef.BottomLeft.x && rover.CurrentPosition.x <= plateauDef.TopRight.x) && 
                (rover.CurrentPosition.y >= plateauDef.BottomLeft.y && rover.CurrentPosition.y <= plateauDef.TopRight.y)) //calculate current position of a rover ONLY IF rover's original position falls under given plateau coordinates
            {
                return true;
            }
            else
            {
                return false; //this means this particular rover's original coordinates do not fall inside given plateau coordinates, hence invalid position of rover
            }
        }

        /// <summary>
        /// Moves the rover as per its defined route inside the plateau.
        /// </summary>
        public void MoveRoversAsPerDirections()
        {
            //Redefining the Plateau bottom-left and top-right coordinates if incorrect coordinates passed
            PlateauDef.Redefine();

            foreach (Rover rover in Rovers) //get current position FOR EACH rover
            {
                if (IsRoverPositionValid(rover, PlateauDef))
                {
                    char[] route = rover.Route.ToUpper().ToCharArray();
                    foreach (char ch in route) //update rover's current position FOR EACH given route instruction
                    {
                        switch (ch)
                        {
                            case 'L':
                                rover.TurnLeft();
                                break;
                            case 'R':
                                rover.TurnRight();
                                break;
                            case 'M':
                                //update rover position only if new rover coordinate falls under given plateau cordinates boundary
                                rover.MoveForward();
                                if (!IsRoverPositionValid(rover, PlateauDef))
                                {
                                    rover.MoveBackward();
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
