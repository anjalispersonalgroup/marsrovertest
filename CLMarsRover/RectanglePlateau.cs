﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLOprPlanetX
{
    public class RectanglePlateau : IPlateau
    {
        private TwoDCoordinate _bottomLeft; //x1, y1
        private TwoDCoordinate _topRight; //x2, y2

        public RectanglePlateau(TwoDCoordinate bottomLeft, TwoDCoordinate topRight)
        {
            _bottomLeft = bottomLeft;
            _topRight = topRight;
        }

        public TwoDCoordinate BottomLeft { get { return _bottomLeft; } }

        public TwoDCoordinate TopRight { get { return _topRight; } }

        /// <summary>
        /// Redefines the Plateau bottom-left and top-right corners if passed coordinates are not in proper order.
        /// </summary>
        public void Redefine()
        {
            int tmp;
            tmp = _bottomLeft.x;
            _bottomLeft.x = (_bottomLeft.x < _topRight.x ? _bottomLeft.x : _topRight.x);
            _topRight.x = (_topRight.x > tmp ? _topRight.x : tmp);
            tmp = _bottomLeft.y;
            _bottomLeft.y = (_bottomLeft.y < _topRight.y ? _bottomLeft.y : _topRight.y);
            _topRight.y = (_topRight.y > tmp ? _topRight.y : tmp);
        }

        public double CalculateArea()
        {
            double area = 0;

            return area;
        }
    }
}
