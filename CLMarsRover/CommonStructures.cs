﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLOprPlanetX
{
    public enum CompassDirection
    {
        N,
        E,
        S,
        W
    }

    public struct TwoDCoordinate
    {
        public int x;
        public int y;

        public TwoDCoordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
