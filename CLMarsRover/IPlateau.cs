﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLOprPlanetX
{
    interface IPlateau
    {
        double CalculateArea();
    }
}
