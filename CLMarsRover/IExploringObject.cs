﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CLOprPlanetX
{
    interface IExploringObject
    {
        void TurnLeft();

        void TurnRight();

        void MoveForward();

        void MoveBackward();
    }
}
