﻿using System;

namespace CLOprPlanetX
{
    public class Rover : IExploringObject
    {
        private TwoDCoordinate _currentPosition;
        private CompassDirection _roverDirection;
        private string _route;
        
        public Rover(TwoDCoordinate currentPosition, CompassDirection roverDirection, string route)
        {
            _currentPosition = currentPosition;
            _roverDirection = roverDirection;
            _route = route;
        }

        public TwoDCoordinate CurrentPosition { get { return _currentPosition; } }
        
        public CompassDirection RoverDirection { get { return _roverDirection; } }
        
        public string Route { get { return _route; } }

        /// <summary>
        /// Changes rover's direction to 90 degree left on the same position.
        /// </summary>
        public void TurnLeft()
        {
            switch (_roverDirection)
            {
                case CompassDirection.N:
                    _roverDirection = CompassDirection.W;
                    break;
                case CompassDirection.E:
                    _roverDirection = CompassDirection.N;
                    break;
                case CompassDirection.S:
                    _roverDirection = CompassDirection.E;
                    break;
                case CompassDirection.W:
                    _roverDirection = CompassDirection.S;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Changes rover's direction to 90 degree right on the same position.
        /// </summary>
        public void TurnRight()
        {
            switch (_roverDirection)
            {
                case CompassDirection.N:
                    _roverDirection = CompassDirection.E;
                    break;
                case CompassDirection.E:
                    _roverDirection = CompassDirection.S;
                    break;
                case CompassDirection.S:
                    _roverDirection = CompassDirection.W;
                    break;
                case CompassDirection.W:
                    _roverDirection = CompassDirection.N;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Moves rover one step further in the current direction.
        /// </summary>
        public void MoveForward()
        {
            switch (_roverDirection)
            {
                case CompassDirection.N:
                    _currentPosition.y = _currentPosition.y + 1; //Move Up
                    break;
                case CompassDirection.E:
                    _currentPosition.x = _currentPosition.x + 1; //Move Right
                    break;
                case CompassDirection.S:
                    _currentPosition.y = _currentPosition.y - 1; //Move Down
                    break;
                case CompassDirection.W:
                    _currentPosition.x = _currentPosition.x - 1; //Move Left
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Moves rover one step back from the current direction.
        /// </summary>
        public void MoveBackward()
        {
            switch (_roverDirection)
            {
                case CompassDirection.N:
                    _currentPosition.y = _currentPosition.y - 1; //Move Down
                    break;
                case CompassDirection.E:
                    _currentPosition.x = _currentPosition.x - 1; //Move Left
                    break;
                case CompassDirection.S:
                    _currentPosition.y = _currentPosition.y + 1; //Move Up
                    break;
                case CompassDirection.W:
                    _currentPosition.x = _currentPosition.x + 1; //Move Right
                    break;
                default:
                    break;
            }
        }
    }
}
